﻿using System;
using System.Collections.Generic;
using System.Text;


namespace LV_1
{

    class Program
    {      

        static void Main(string[] args)
        {
            
            Zabiljeska prva = new Zabiljeska();
            Zabiljeska druga = new Zabiljeska("Bruno Ilić", "Izrada zadatka danas u 17:00", "visoko");
            Zabiljeska treca = new Zabiljeska(druga);
            Biljeska cetvrta = new Biljeska("Bruno Šimunović", "Izrada zadatka danas u 17:00", "visoko");
            ToDoList peta = new ToDoList();
            List<IBilezi> bilezi = new List<IBilezi>();

            Console.WriteLine(prva.Autor + " napisao je: " + prva.Biljeske);
            Console.WriteLine(druga.Autor + " napisao je: " + druga.Biljeske);
            Console.WriteLine(treca.Autor + " napisao je: " + treca.Biljeske);
            Console.WriteLine(cetvrta.ToString());
            Console.WriteLine("\n");

            bilezi.Add(peta);
            
            for(int i = 0; i < 3; i++)
            {
                Zabiljeska novi = new Zabiljeska("Bruno Šimunović");
                novi.Biljeske = Console.ReadLine();
                novi.Razina_vaznosti = Console.ReadLine();
                peta.Dodaj(novi);
            }

            Console.WriteLine(peta.ToString());
            List<Zabiljeska> toRemove = new List<Zabiljeska>();

            foreach (Zabiljeska element in peta.Biljeske)
            {
                if (element.Razina_vaznosti == "visoko")
                {
                    toRemove.Add(element);
                }
            }

            foreach(Zabiljeska element in toRemove)
            {
                peta.Obrisi(element);
            }

            Console.WriteLine(peta.ToString());


        }

        



    }



}
