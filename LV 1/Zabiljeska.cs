﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_1
{
    class Zabiljeska
    {
        #region private

        private String autor;
        private String biljeske;
        private String razina_Vaznosti;

        #endregion

        #region public
        public Zabiljeska()
        {
            autor = "Annonymus";
            biljeske = "No message!";
            razina_Vaznosti = "nisko";
        }

        public Zabiljeska(string autor, string biljeske, string razina_vaznosti)
        {
            this.autor = autor;
            this.biljeske = biljeske;
            this.razina_Vaznosti = razina_vaznosti;
        }

        public Zabiljeska(Zabiljeska kopija)
        {
            autor = kopija.autor;
            biljeske = kopija.biljeske;
            razina_Vaznosti = kopija.razina_Vaznosti;
        }

        public Zabiljeska(string autor)
        {
            this.autor = autor;
            biljeske = "No message!";
            razina_Vaznosti = "nisko";
        }

        public string getAutor() { return autor; }
        public string getBiljeske() { return biljeske; }
        public string getRazina_Vaznosti() { return razina_Vaznosti; }
        public void setBiljeske(string name) { biljeske = name; }
        public void setRazina_Vaznosti(string surname) { razina_Vaznosti = surname; }


        public string Autor
        {
            get { return autor; }
            private set { autor = value; }
        }

        public string Biljeske
        {
            get { return biljeske; }
            set { biljeske = value; }
        }

        public string Razina_vaznosti
        {
            get { return razina_Vaznosti; }
            set { razina_Vaznosti = value; }
        }

        public override string ToString()
        {
            return autor + " napisao je: " + biljeske + " Razine vaznosti: " + razina_Vaznosti;
        }
        #endregion
    }
}
