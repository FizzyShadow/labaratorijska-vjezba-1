﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_1
{
    
    class ToDoList : IBilezi
    {
        #region private

        private List<Zabiljeska> biljeske;

        #endregion



        #region public

        public List<Zabiljeska> Biljeske
        {
            get { return biljeske; }
            set { biljeske = value; }
        }

        public ToDoList()
        {
            biljeske = new List<Zabiljeska>();
        }

        public void Dodaj(Zabiljeska biljeska)
        {
            biljeske.Add(biljeska);
        }

        public void Obrisi(Zabiljeska zabiljeska)
        {
            biljeske.Remove(zabiljeska);
        }

        public Zabiljeska Dohvati(int indexZabiljeske)
        {
            return biljeske[indexZabiljeske];
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach(Zabiljeska zabiljeska in biljeske)
            {
                builder.Append(zabiljeska).Append(System.Environment.NewLine);
            }
            return builder.ToString();
        }



        #endregion

    }
}
