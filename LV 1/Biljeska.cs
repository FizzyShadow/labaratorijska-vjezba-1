﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_1
{
    class Biljeska : Zabiljeska
    {
        #region private

        private DateTime trenutnoVrijeme;

        #endregion

        #region public

        public Biljeska(string autor, string biljeske, string razina_Vaznosti) : base(autor, biljeske, razina_Vaznosti)
        {
            this.trenutnoVrijeme = DateTime.Now;
        }

        public DateTime TrenutnoVrijeme
        {
            get { return trenutnoVrijeme; }
            set { trenutnoVrijeme = value; }
        }

        public override string ToString()
        {
            return base.ToString() + " Napisano u vremenu: " + trenutnoVrijeme;
        }

        #endregion
    }
}
