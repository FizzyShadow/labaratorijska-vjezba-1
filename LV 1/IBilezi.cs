﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_1
{
    interface IBilezi
    {
        void Dodaj(Zabiljeska biljeska);
        void Obrisi(Zabiljeska zabiljeska);
        Zabiljeska Dohvati(int brojZabiljeske);
        string ToString();
    }
}
